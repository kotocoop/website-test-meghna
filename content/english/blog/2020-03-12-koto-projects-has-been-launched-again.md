---
layout: post
title: Koto projects has been launched again!
sub_heading: ''
date: 2020-03-12 17:19:36 +0000
tags: []
banner_image: ''
related_posts: []

---
TZM Finland has decided to make Koto it's main project for this year. 

We have almost founded the official coop in Finland and we are looking into to founding the first unit in summer 2020.

If you wan't to be part of the project, read this document [https://cloud.kotocoop.org/index.php/s/e4XXWwsdJwA8fxz](https://cloud.kotocoop.org/index.php/s/e4XXWwsdJwA8fxz "https://cloud.kotocoop.org/index.php/s/e4XXWwsdJwA8fxz")
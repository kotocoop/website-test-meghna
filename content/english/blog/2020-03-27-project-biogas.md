---
layout: post
title: 'Project: Biogas'
sub_heading: ''
date: 2020-03-27 22:00:00 +0000
tags: [project, biogas]
banner_image: ''
related_posts: []

---
As our first project we decided to look into generating heating energy and electricity with home made biogas.

We have had three meetings and here are the notes:

* 28.3.2020 [https://cloud.kotocoop.org/index.php/s/Hsx3G6AYG9YGwqZ](https://cloud.kotocoop.org/index.php/s/Hsx3G6AYG9YGwqZ "https://cloud.kotocoop.org/index.php/s/Hsx3G6AYG9YGwqZ")
* 4.4.2020 [https://cloud.kotocoop.org/index.php/s/s7DjSDAT2TEcLYd](https://cloud.kotocoop.org/index.php/s/s7DjSDAT2TEcLYd "https://cloud.kotocoop.org/index.php/s/s7DjSDAT2TEcLYd")

We decided to also start to look into building or buying a CNC -machine. These will be separate projects and will have their own meetings.
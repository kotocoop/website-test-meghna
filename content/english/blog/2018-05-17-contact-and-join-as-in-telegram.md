---
title: Join us on Telegram
layout: post
date: 2018-05-17 05:41:02 +0000
sub_heading: Join us on Telegram
tags: []
banner_image: ''
image: images/blog/meghna.jpg
related_posts: []
---
## TELEGRAM GROUPS

In effort to ditch Facebook we created two Telegram groups. Groups are "Koto coop" and more general group hopefully shared between similar projects called "Creating hi-tech NL/RBE communities". Send a message to Juuso to get an invite [https://t.me/jeukku](https://t.me/jeukku) (no direct invite links anymore because there has been too much spam).

## DISCORD SERVER

There is also a discord server you can use. Invitation link here: [https://discord.gg/7eVJFCG](https://discord.gg/7eVJFCG "https://discord.gg/7eVJFCG") (say hi ti to jeukku (Juuso V) :) )

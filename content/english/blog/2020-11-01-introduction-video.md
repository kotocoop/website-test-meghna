---
layout: post
title: 'Introduction video'
sub_heading: ''
date: 2020-11-01 14:30:00 +0000
tags: []
banner_image: ''
related_posts: []

---

Koto Coop Introduction video has been now published!

The idea of Koto Coop started a couple of years ago in Finland and is a grassroots initiative for a sustainable future where we share our resources, meaning both our materials and our time. With the goal of the well being of our human species being united and in cohabitation with our environment. Instead of the competitive, profit seeking incentive for infinite economic growth, to the detriment of both our individual and our collective health alike. With this in mind the purpose of Koto Coop is to develop and implement sustainable technologies and social structures that focus on making this vision a reality.

<iframe width="560" height="315" src="https://www.youtube.com/embed/K_UBsWDjYaw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[https://www.youtube.com/watch?v=K_UBsWDjYaw](https://www.youtube.com/watch?v=K_UBsWDjYaw)

---
layout: post
title: 'Starting funding in teams'
sub_heading: ''
date: 2020-07-18 22:00:00 +0000
tags: []
banner_image: ''
related_posts: []

---

While we feel like it would be nice to boycott money, 
we think that more effective way is to gather money and other resouces in an co-op and share them to members of that co-op. 
This makes members free to focus on more important things than just getting paid to survive. 
We can also provide work for people who will not or cannot take part in any tasks or jobs closely involved with the monetary system. 
This type of work is for example taking care of the land, buildings, persons and plants.

When the co-op grows, less involvement with money is needed because we can produce more and more what we need inside the co-op.  
It is one of the organizations that pushes forward "price of zero" transition. 
Check out Michaels excellent presentation of it here [https://www.youtube.com/watch?time_continue=5&v=0Ogy7sCAfJY](https://www.youtube.com/watch?time_continue=5&v=0Ogy7sCAfJY)

Join our our Telegram channel and group through that: [https://t.me/kotocoop](https://t.me/kotocoop)

## Starting the funding
In meeting yesterday we concluded that we will look more into five different ways to make money for Koto Coop. We have created four teams to move forward with these ideas. If you think that you would like to work on these matters please join these team chats.

### Development team, responsible person Jeukku
This team finds out what software developent, system specialists, website hosting services, game development or other project we could provide as a business. With these services we can combine remote work with eco-village style living.

### Workshop team, responsible person Juuso                     
This team is looking into how to use and create machines that we can use for creating and assembling things like FarmBots and hydroponic indoors farming systems etc. and if there is a possibility for example build and install them as a service.

### Youtube team, responsible person Tuomas 
For near future good Youtube (and other platforms) visiblity is essential to share our story to look like a strustworthy organization and possibly get more support for growdfunding campains etc.  

### Online Courses team, responsible person Simon
This team looks for options to host online courses and workshops on various topics.

### Other
And also extra money can come from donations, crowdfunding campaings and eventually from loans or grants.

### Links to team groups
If you think you could in one of the teams, 
you can find links to team groups in this [document](https://cloud.kotocoop.org/index.php/s/yxpg9sCS65qainn)
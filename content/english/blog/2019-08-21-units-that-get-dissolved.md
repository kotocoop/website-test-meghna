---
title: When units gets dissolved to make Koto work in a large scale
layout: post
date: 2019-08-21T12:41:02.000+00:00
tags: []
author: Juuso
banner_image: ''
related_posts: []

---
Hey, Juuso here :) In my mind Koto will be a network of small units pretty long. A good part of Phase 2 in the description document.
A unit is let's say at max twenty people. Units can be built next to each other but it is probably for the best if only one per city is created at first.

Since growth is the main goal in Koto we cannot be too nice about trying to keep those units alive. An unit has to make some profit or at least run on it's own. If it doesn't Koto has wasted money on buying the land where the unit is.
So there needs to be a process of dissolving an unit. Something like this:

* an unit requests repeadetly resources from Koto
* reports will be made and studied and based on that a warning is sent to people living in the unit
* If things doesn't get better, Koto decides that there is no good reason to keep on supporting the unit.
* people living in that unit will be notified that the unit will be taken down, property will be sold or moved and they have six months to deal with it
* people can then take a few different actions like moving to a different unit if they are accepted
* people in the unit are dismissed. Participitory share is refunded following the rules (in six months or something like that).
* based on the situation Koto can the decide to sell the land and property or start a new unit there.

That, combined to the idea that different units might have slightly different rules, might make this whole process evolutionary in a way.

Also I should point out that it might sound harsh but it goes with the idea that there will be tens of thousands of units and the number is growing fast. We can handle moving resources between that number of units when it is needed, but centrally taking care of all their needs is impossible.

Since times are dire when you think about the state of the earth growth is the most important aspect to me. Growth of couse means here the number of units. Fast growth leads to using less resources world wide and minimizing the effect of the eco-catastrophe happening.

But all this is of couse an issue when Koto is actually started and growing fast. Next post will hopefully be about different ideas about actually getting started.

\- Juuso